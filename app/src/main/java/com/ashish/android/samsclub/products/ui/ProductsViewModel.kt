package com.ashish.android.samsclub.products.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ashish.android.samsclub.products.domain.ProductsDataSource
import com.ashish.android.samsclub.products.domain.ProductsDataSourceFactory
import com.ashish.android.samsclub.products.domain.State
import com.ashish.android.samsclub.products.network.model.Product
import javax.inject.Inject

class ProductsViewModel @Inject constructor(private val productsDataSourceFactory: ProductsDataSourceFactory) :
    ViewModel() {

    private val pageSize = 10

    var productsList: LiveData<PagedList<Product>>

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()
        productsList = LivePagedListBuilder(this.productsDataSourceFactory, config).build()
    }

    fun getState(): LiveData<State> {
        return Transformations.switchMap<ProductsDataSource,
                State>(productsDataSourceFactory.productsDataSourceLiveData, ProductsDataSource::state)
    }

    fun isEmptyList(): Boolean {
        return productsList.value?.isEmpty() ?: true
    }
}