package com.ashish.android.samsclub.products.domain

enum class State {
    DONE, LOADING, ERROR
}