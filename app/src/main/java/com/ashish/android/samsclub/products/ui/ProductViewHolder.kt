package com.ashish.android.samsclub.products.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ashish.android.samsclub.core.getCompleteImageUrl
import com.ashish.android.samsclub.databinding.ProductItemRowBinding
import com.ashish.android.samsclub.products.network.model.Product
import com.squareup.picasso.Picasso

class ProductViewHolder(val binding: ProductItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(product: Product?, listener: (Product) -> Unit) {
        if (product != null) {
            Picasso.get().load(getCompleteImageUrl(product.productImage)).into(binding.productIv)
            binding.productPriceTv.text = product.price
            binding.productNameTv.text = product.productName
            binding.root.setOnClickListener { listener(product) }
        }
    }

    companion object {
        fun create(parent: ViewGroup): ProductViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ProductItemRowBinding.inflate(inflater)
            return ProductViewHolder(binding)
        }
    }
}