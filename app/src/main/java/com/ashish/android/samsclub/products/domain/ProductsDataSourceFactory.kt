package com.ashish.android.samsclub.products.domain

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.ashish.android.samsclub.products.network.ProductsService
import com.ashish.android.samsclub.products.network.model.Product
import javax.inject.Inject

class ProductsDataSourceFactory @Inject constructor(
    private val productsService: ProductsService
) : DataSource.Factory<Int, Product>() {

    val productsDataSourceLiveData = MutableLiveData<ProductsDataSource>()

    override fun create(): DataSource<Int, Product> {
        val productsDataSource = ProductsDataSource(productsService)
        productsDataSourceLiveData.postValue(productsDataSource)
        return productsDataSource
    }
}