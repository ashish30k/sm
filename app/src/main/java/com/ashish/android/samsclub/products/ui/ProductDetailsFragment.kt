package com.ashish.android.samsclub.products.ui

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.ashish.android.samsclub.R
import com.ashish.android.samsclub.core.getCompleteImageUrl
import com.ashish.android.samsclub.databinding.FragmentProductDetailsBinding
import com.ashish.android.samsclub.products.network.model.Product
import com.squareup.picasso.Picasso

const val PRODUCT_BUNDLE_EXTRA = "product_bundle_extra"

class ProductDetailsFragment : Fragment() {
    private lateinit var binding: FragmentProductDetailsBinding
    private var product: Product? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments.let {
            product = it?.getParcelable(PRODUCT_BUNDLE_EXTRA)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_product_details,
            container,
            false
        )
        binding.product = product

        product?.let { product ->
            Picasso.get().load(getCompleteImageUrl(product.productImage)).into(binding.productDetailsIv)
            binding.productDetailsNameTv.text = product.productName

            binding.productDetailsPriceTv.text = product.price

            if (!product.shortDescription.isNullOrEmpty()) {
                binding.productDetailsShortDescriptionTv.text = Html.fromHtml(product.shortDescription)
            }

            if (!product.shortDescription.isNullOrEmpty()) {
                binding.productDetailsLongDescriptionTv.text = Html.fromHtml(product.longDescription)
            }

            binding.productDetailsRb.rating = product.reviewRating

            binding.productDetailsReviewsCountTv.text =
                String.format(getString(R.string.product_rating_count), product.reviewCount)
        }

        return binding.root
    }
}