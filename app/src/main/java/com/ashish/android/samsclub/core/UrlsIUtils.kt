package com.ashish.android.samsclub.core

import androidx.annotation.NonNull

const val BASE_URL = "https://mobile-tha-server.firebaseapp.com/"

fun getCompleteImageUrl(@NonNull url: String): String {
    return BASE_URL + url
}