package com.ashish.android.samsclub.products.dagger

import com.ashish.android.samsclub.core.dagger.ActivityScope
import com.ashish.android.samsclub.products.ui.ProductsActivity
import com.ashish.android.samsclub.products.ui.ProductsFragment
import dagger.Subcomponent


@ActivityScope
@Subcomponent
interface ProductsComponent {
    fun inject(restaurantsActivity: ProductsActivity)
    fun inject(restaurantsFragment: ProductsFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(): ProductsComponent
    }
}