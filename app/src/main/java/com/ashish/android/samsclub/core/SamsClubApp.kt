package com.ashish.android.samsclub.core

import android.app.Application
import com.ashish.android.samsclub.core.dagger.AppComponent
import com.ashish.android.samsclub.core.dagger.AppModule
import com.ashish.android.samsclub.core.dagger.DaggerAppComponent

class SamsClubApp : Application() {
    lateinit var appComponent: AppComponent
    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        appComponent.inject(this)
    }
}