package com.ashish.android.samsclub.products.domain

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.ashish.android.samsclub.products.network.ProductsService
import com.ashish.android.samsclub.products.network.model.Product
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductsDataSource @Inject constructor(
    private val productsService: ProductsService
) : PageKeyedDataSource<Int, Product>() {

    var state: MutableLiveData<State> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Product>) {
        val uiScope = CoroutineScope(Dispatchers.Main)

        uiScope.launch {
            state.postValue(State.LOADING)
            try {
                withContext(Dispatchers.IO) {
                    val productsResponse = productsService.getProducts(1, params.requestedLoadSize)
                    callback.onResult(productsResponse.products, null, 2)
                }
            } catch (e: Exception) {
                state.postValue(State.ERROR)
            }
            state.postValue(State.DONE)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
        val uiScope = CoroutineScope(Dispatchers.Main)

        uiScope.launch {
            state.postValue(State.LOADING)
            withContext(Dispatchers.IO) {
                val productsResponse = productsService.getProducts(params.key, params.requestedLoadSize)
                callback.onResult(productsResponse.products, params.key + 1)
            }
            state.postValue(State.DONE)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
    }
}