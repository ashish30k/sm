package com.ashish.android.samsclub.products.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ashish.android.samsclub.core.dagger.ActivityScope
import com.ashish.android.samsclub.products.domain.ProductsDataSourceFactory
import javax.inject.Inject

@ActivityScope
class ProductsViewModelFactory @Inject constructor(val productsDataSourceFactory: ProductsDataSourceFactory) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProductsViewModel::class.java)) {
            return ProductsViewModel(productsDataSourceFactory) as T
        }
        throw Throwable("Unknown ViewModel class")
    }
}