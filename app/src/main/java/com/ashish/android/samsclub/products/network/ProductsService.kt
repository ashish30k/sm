package com.ashish.android.samsclub.products.network

import com.ashish.android.samsclub.products.network.model.ProductsResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductsService {
    @GET("/walmartproducts/{pageNumber}/{pageSize}")
    suspend fun getProducts(@Path("pageNumber") pageNumber: Int, @Path("pageSize") pageSize: Int): ProductsResponse
}