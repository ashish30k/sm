package com.ashish.android.samsclub.products.ui

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ashish.android.samsclub.products.domain.State
import com.ashish.android.samsclub.products.network.model.Product

class ProductsListAdapter(val productClickListener: ((Product) -> Unit)) :
    PagedListAdapter<Product, RecyclerView.ViewHolder>(ProductsDiffCallback) {

    private val PRODUCT_VIEW_TYPE = 1
    private val PROGRESS_BAR_VIEW_TYPE = 2

    private var state = State.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == PRODUCT_VIEW_TYPE) {
            return ProductViewHolder.create(parent)
        } else {
            return ProgressBarViewHolder.create(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == PRODUCT_VIEW_TYPE) {
            (holder as ProductViewHolder).bind(getItem(position), productClickListener)
        } else {
            (holder as ProgressBarViewHolder).bind(state)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position < super.getItemCount()) {
            return PRODUCT_VIEW_TYPE
        } else {
            return PROGRESS_BAR_VIEW_TYPE
        }
    }

    companion object {
        val ProductsDiffCallback = object : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(oldProduct: Product, newProduct: Product): Boolean {
                return oldProduct.productId == newProduct.productId
            }

            override fun areContentsTheSame(oldProduct: Product, newProduct: Product): Boolean {
                return oldProduct == newProduct
            }
        }
    }

    override fun getItemCount(): Int {
        if (hasFooter()) {
            return super.getItemCount() + 1
        } else {
            return super.getItemCount()
        }
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }
}