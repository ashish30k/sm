package com.ashish.android.samsclub.core.dagger

import com.ashish.android.samsclub.products.dagger.ProductsComponent
import dagger.Module

@Module(subcomponents = [ProductsComponent::class])
class SubComponentsModule {
}