package com.ashish.android.samsclub.products.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    @SerializedName("productId") val productId: String,
    @SerializedName("productName") val productName: String = "",
    @SerializedName("shortDescription") val shortDescription: String = "",
    @SerializedName("longDescription") val longDescription: String = "",
    @SerializedName("price") val price: String,
    @SerializedName("productImage") val productImage: String = "",
    @SerializedName("reviewRating") val reviewRating: Float =0.0F,
    @SerializedName("reviewCount") val reviewCount: Int=0,
    @SerializedName("inStock") val inStock: Boolean = false
) : Parcelable {
}