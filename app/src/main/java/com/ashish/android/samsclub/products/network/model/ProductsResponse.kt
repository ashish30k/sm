package com.ashish.android.samsclub.products.network.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductsResponse(
    @SerializedName("products") val products: List<Product>,
    @SerializedName("totalProducts") val totalProducts: Int,
    @SerializedName("pageNumber") val pageNumber: Int,
    @SerializedName("pageSize") val pageSize: Int,
    @SerializedName("statusCode") val statusCode: Int
) : Parcelable