package com.ashish.android.samsclub.products.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ashish.android.samsclub.R
import com.ashish.android.samsclub.core.SamsClubApp
import com.ashish.android.samsclub.databinding.FragmentProductsBinding
import com.ashish.android.samsclub.products.dagger.ProductsComponent
import com.ashish.android.samsclub.products.domain.ProductsDataSourceFactory
import com.ashish.android.samsclub.products.domain.State
import com.ashish.android.samsclub.products.network.ProductsService
import com.ashish.android.samsclub.products.network.model.Product
import javax.inject.Inject

class ProductsFragment : Fragment() {

    private lateinit var productsComponent: ProductsComponent

    @Inject
    lateinit var productsService: ProductsService

    private lateinit var productsViewModel: ProductsViewModel

    private lateinit var productsDataSourceFactory: ProductsDataSourceFactory

    private lateinit var productsListAdapter: ProductsListAdapter

    private lateinit var binding: FragmentProductsBinding

    override fun onAttach(context: Context) {
        productsComponent = (context.applicationContext as SamsClubApp).appComponent.restaurantsComponent().create()
        productsComponent.inject(this)

        super.onAttach(context)

        productsDataSourceFactory = ProductsDataSourceFactory(productsService)

        productsViewModel =
            ViewModelProviders.of(this, ProductsViewModelFactory(productsDataSourceFactory))
                .get(ProductsViewModel::class.java)

        if (context is OnProductSelectListener) {
            callback = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_products,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.title = getString(R.string.products_activity_title)

        setupAdapter()
        observeLiveData()
    }

    private fun observeLiveData() {
        productsViewModel.productsList.observe(viewLifecycleOwner, Observer {
            productsListAdapter.submitList(it)

            val adapterPosition: Int? =
                (context?.applicationContext as SamsClubApp).appComponent.getSharedPrefsHelper()
                    .getInt(SAVED_SCROLL_POSITION_PREF_KEY)

            binding.productsRv.scrollToPosition(adapterPosition!!)
        })

        productsViewModel.getState().observe(viewLifecycleOwner, Observer { state ->
            if (productsViewModel.isEmptyList() && state == State.LOADING) {
                binding.loadingProgressBar.visibility = View.VISIBLE
            } else {
                binding.loadingProgressBar.visibility = View.GONE
            }

            if (productsViewModel.isEmptyList() && state == State.ERROR) {
                binding.errorTv.visibility = View.VISIBLE
            } else {
                binding.errorTv.visibility = View.GONE
            }

            if (!productsViewModel.isEmptyList()) {
                productsListAdapter.setState(state ?: State.DONE)
            }
        })
    }

    private fun setupAdapter() {
        val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        binding.productsRv.addItemDecoration(decoration)

        val layoutManager = LinearLayoutManager(this.context)
        binding.productsRv.layoutManager = layoutManager

        productsListAdapter = ProductsListAdapter {
            saveScrollPosition()
            callback?.onProductSelected(it)
        }

        binding.productsRv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.productsRv.adapter = productsListAdapter
    }

    override fun onStop() {
        super.onStop()
        saveScrollPosition()
    }

    private fun saveScrollPosition() {
        (context?.applicationContext as SamsClubApp).appComponent.getSharedPrefsHelper().putInt(
            SAVED_SCROLL_POSITION_PREF_KEY,
            (binding.productsRv.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        )
    }

    interface OnProductSelectListener {
        fun onProductSelected(product: Product)
    }

    private var callback: OnProductSelectListener? = null

    fun setOnProductSelectListener(callback: OnProductSelectListener) {
        this.callback = callback
    }
}