package com.ashish.android.samsclub.core.dagger

import android.app.Application
import android.content.Context
import com.ashish.android.samsclub.core.SamsClubApp
import com.ashish.android.samsclub.core.SharedPrefsHelper
import com.ashish.android.samsclub.products.dagger.ProductsComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, AppModule::class, SubComponentsModule::class])
interface AppComponent {
    fun inject(samsClubApp: SamsClubApp)
    fun getAppContext(): Context
    fun getApplication(): Application
    fun getSharedPrefsHelper(): SharedPrefsHelper
    fun restaurantsComponent(): ProductsComponent.Factory
}