package com.ashish.android.samsclub.products.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.ashish.android.samsclub.R
import com.ashish.android.samsclub.core.SamsClubApp
import com.ashish.android.samsclub.core.replaceFragmentToBackStack
import com.ashish.android.samsclub.databinding.ActivityProductsBinding
import com.ashish.android.samsclub.products.network.model.Product

const val SAVED_SCROLL_POSITION_PREF_KEY = "saved_scroll_position_pref_key"

class ProductsActivity : AppCompatActivity(), ProductsFragment.OnProductSelectListener {

    private lateinit var binding: ActivityProductsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_products)

        if (savedInstanceState == null) {
            (this.applicationContext as SamsClubApp).appComponent.getSharedPrefsHelper()
                .putInt(SAVED_SCROLL_POSITION_PREF_KEY, 0)
            replaceFragmentToBackStack(R.id.products_fragment_container, ProductsFragment())
        }
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        if (fragment is ProductsFragment) {
            fragment.setOnProductSelectListener(this)
        }
    }

    override fun onProductSelected(product: Product) {
        val bundle = Bundle()
        bundle.putParcelable(PRODUCT_BUNDLE_EXTRA, product)

        val productDetailsFragment = ProductDetailsFragment()
        productDetailsFragment.arguments = bundle
        replaceFragmentToBackStack(R.id.products_fragment_container, productDetailsFragment)
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        when {
            count == 1 -> finish()
            count > 1 -> supportFragmentManager.popBackStack()
            else -> super.onBackPressed()
        }
    }
}
